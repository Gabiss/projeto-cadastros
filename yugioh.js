document.querySelector('#gerarCarta').addEventListener('click',getCard);

async function getCard() {
    const carta = document.querySelector("#cartaEscolhida")
  
    const linkCarta = await fetch(`https://db.ygoprodeck.com/api/v7/cardinfo.php?name=${carta.value}`);
    const dados = await linkCarta.json();

    const elemCarta = document.createElement('p')
    elemCarta.innerHTML= `<p>${dados.data[0].name}</p>
    <img src= ${dados.data[0].card_images[0].image_url_small}></img>
    <p>${dados.data[0].race} ${dados.data[0].type}</p>
    <p>${dados.data[0].desc}</p>
    `
    document.body.appendChild(elemCarta)
    console.log(dados);
}

document.querySelector('#gerarCarta2').addEventListener('click',getRandomCard);

async function getRandomCard() {

    const n1 = Math.floor(Math.random() * 9999)

    const linkCarta = await fetch(`https://db.ygoprodeck.com/api/v7/cardinfo.php`);
    const dados = await linkCarta.json();

    const elemCarta = document.createElement('p')
    elemCarta.innerHTML= `<p>${dados.data[n1].name}</p>
    <img src= ${dados.data[n1].card_images[0].image_url_small}></img>
    <p>${dados.data[n1].race} / ${dados.data[n1].type}</p>
    <p>${dados.data[n1].desc}</p>
    `

    document.body.appendChild(elemCarta)
    console.log(dados);
}


// fazer um Math.floor(Math.random() * 20 só q com 9 caracteres